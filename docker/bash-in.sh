#!/bin/bash

source ./vars.sh

if [ "x$1" = "x" ] ; then
	echo "Please specify which container you wish to access, either 'httpd' or 'db'."
	exit 1
fi

echo ===========================================================================================
docker exec -it ${ATCS_PROJECT_NAME}_${1}_1 /bin/bash
