#!/bin/bash

## Docker image:  hyphenated image name, based on domain name, e.g. example-com
## Project name:  short name for httpd service, such as brand name ("mycompany") or generic ("publicweb").
## 	(Project name becomes basis for Docker network names, etc)
## App domain:  full primary domain name upon which the web service will respond
## Client code:  client code/account number (basis for multiple domains under one client folder, if you're hosting that way)
## Password: Becomes password for all services admins/logins

export ATCS_PROJECT_NAME=exampleapp
export ATCS_APP_DOMAIN=example.com
export ATCS_CLIENT_CODE=exa001
export ATCS_CLIENT_PASSWORD="STRONG_PW_HERE"