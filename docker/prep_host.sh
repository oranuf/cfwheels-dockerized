
#!/bin/bash

# This script is meant to set up the HOST for docker containers

source ./vars.sh

# set up docker/repository on HOST
# taken from https://docs.docker.com/install/linux/docker-ce/debian/#set-up-the-repository

apt-get -y update

apt-get install -y \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg2 \
	software-properties-common \
	git

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

echo "Match this fingerprint with 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88":
apt-key fingerprint 0EBFCD88

add-apt-repository \
	"deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
	$(lsb_release -cs) \
	stable"

# get docker

apt-get -y update

apt-get install -y docker-ce=17.12.1~ce-0~debian

docker run hello-world

# start docker on boot

systemctl enable docker

apt-get install -y docker-compose

# set up environment for git, and create base directory structure

cp ./gitignore ../.gitignore

# docker bind volume directories
# DO WE NEED THIS IF SIMPLY BUILDING LOCALLY AND PUSHING TO GIT REPO?  The entire project will be there...
# maybe just leave these and let them error
cd ..

mkdir _docs_

mkdir db
	mkdir db/data
	mkdir db/logs
	mkdir db/snapshots

mkdir gitignored

mkdir httpd
	mkdir httpd/logs
		mkdir httpd/logs/ols
			mkdir httpd/logs/ols/server
			mkdir httpd/logs/ols/web
		mkdir httpd/logs/tomcat
		mkdir httpd/logs/lucee
			mkdir httpd/logs/lucee/server
			mkdir httpd/logs/lucee/web
		mkdir httpd/logs/app
	mkdir httpd/cache
	mkdir httpd/www

# rm ./docker.zip

cat << EOT >> /root/.bashrc

alias ls='clear && pwd && echo ===========================================================================================&& ls -AFhlp --color=auto --group-directories-first'
alias dirs='dirs -v'
alias dki='docker images'
alias dkc='docker container ls -a'
alias bi='/home/${ATCS_CLIENT_CODE}/${ATCS_APP_DOMAIN}/docker/httpd/bash-in.sh'
alias down='/home/${ATCS_CLIENT_CODE}/${ATCS_APP_DOMAIN}/docker/down.sh'
alias up='/home/${ATCS_CLIENT_CODE}/${ATCS_APP_DOMAIN}/docker/up.sh'

cd /home/${ATCS_CLIENT_CODE}/${ATCS_APP_DOMAIN}/docker

EOT

