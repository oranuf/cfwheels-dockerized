#!/bin/bash

source ../vars.sh


echo ===========================================================================================
docker container stop ${ATCS_PROJECT_NAME}_db_1
docker system prune -f
docker rmi ${ATCS_PROJECT_NAME}-db
