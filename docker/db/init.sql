
CREATE DATABASE IF NOT EXISTS `exampleapp` DEFAULT CHARACTER SET latin1 DEFAULT COLLATE latin1_swedish_ci;
USE `exampleapp`;

CREATE TABLE `stack_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stack_item` varchar(128) DEFAULT 'NULL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `stack_info` (`id`, `stack_item`) VALUES 
	(1,'Debian 9.3'),
	(2,'OpenLiteSpeed 1.4.30'),
	(3,'Lucee 5.2.6.060'),
	(4,'CFWheels 2.0.1'),
	(5,'MariaDB 10.3.5 on Debian 8');
	