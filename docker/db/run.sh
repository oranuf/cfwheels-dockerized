#!/bin/bash

source ../vars.sh

echo ===========================================================================================
docker container prune -f
docker run -d --name ${ATCS_PROJECT_NAME}_db_1 -p 3306:3306  ${ATCS_PROJECT_NAME}-db
