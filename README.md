# CFWheels - Dockerized!

With this repo, and a local docker install, you should be able to get up and running with CFWheels, one of the best frameworks for ColdFusion-based websites,
atop Lucee, the best open-source ColdFusion server, atop OpenLiteSpeed, the fastest caching/proxy/web server available, atop Debian, "The Universal Operating System."

Depending on your internet connection speed, you can be up and running with a complete development/production environment (one and the same, with Docker!) in under
ten minutes!

## Docker Image: Web Service ("httpd")

### Open Ports

* 80
* 443
* 7080 (OLS Admin)
* 8088 (OLS Example Page/PHP Info)
* 8888 (Lucee Admin)

### Debian v9.3

* http://debian.org

### OpenLiteSpeed v1.4.30 / PHP v7.2.1

* http://open.litespeedtech.com
* http://php.net
* OpenLiteSpeed Admin: http://{SERVER_IP}:7080 - you must use server IP address

### Lucee 5.2.6.060

* http://lucee.org
* Lucee Admin: http://{SERVER_IP}:8888/lucee/admin/server.cfm - you must use server IP address


### CFWheels v2.0.1

* http://cfwheels.org

## Docker Image: Database Service ("db")

### Open Ports

* 3306

### Debian v8

* http://debian.org

### MariaDB 10.3.5

* https://mariadb.org/

### From Empty Directory to CFWheels Development In Under 10 Minutes

* Presently, there are only scripts available for Linux/MacOS based machines. If you're running Windows, you'll probably need to dissect the .sh scripts and mimick what they
	do manually. (which actually isn't that hard, they are pretty much all DOCKER commands anyway.)

* Install GIT and DOCKER on your system.

* Clone the repository to a directory on your machine

* Modify /docker/vars.sh

* Set up your local HOSTS file to map your project's domain name to 127.0.0.1

* Run /docker/up.sh.  This will execute a build of all two docker containers (httpd and db) and run them

* Connect your preferred MySQL client to localhost; access via root, with the password you set in vars.sh. You will see a new database with the same name as you set in vars.sh project name.
	NOTE:  If accessing remotely, you will need to take extra steps to allow root access from a remote location. {TODO: STEPS TO BE INCLUDED BELOW}

* You should now be able to browse to your domain name, and open both the OLS and Lucee administrator sites. Your CFWheels app has already had the datasource information included.
	The welcome page will appear, including your current stack information as pulled from the new database stack_info table.

* If you want to add domain aliases (i.e. application set up at example.com but you also want responses at www.example.com) you'll need to add it as follows:

	* In OLS admin: Virtual Host > General > Domain Aliases > add www.example.com

	* Bash-in to httpd, and modify /opt/lucee/tomcat/conf/server.xml : find the host entry block at the bottom of the file, and add the following inside it:
		<Alias>www.example.com</Alias>

* Start building! Your CFWheels app is in /httpd/www, and you have a full set of log files in /httpd/logs directory for debugging

* You'll use similar steps to deploy your app to production, primarily using git, and there demo scripts in /httpd/www/miscellaneous/gitscripts
	for automated deployment when tagged code is commited to BitBucket (see /miscellaneous/gitscripts/bitbucket-webhook.cfm comments for details)

* This installation has not been fully hardended for security. Use is at your own risk from here.	


## Your Startup Configuration

### Logging

For your convenience, Docker Compose is configured to bind mount certain log directories in the Docker containers to directories in this project's /https/logs 
directory on the web host. Review the docker-compose.yml file for details.

### Notes:

* Main Lucee web application is available on port 443, and it uses the same SSL certificate as the OLS server. You will need to replace this with a proper purchased SSL certificate
if the public will be accessing the site directly instead of through an SSL-providing reverse proxy like CloudFlare.

* SES URLs for CFWheels is enabled by default.  This can be turned off in the CFWheels /config/settings.cfm file. The associated rules for SES URLs have been created for you
in the OpenLiteSpeed vhost context.

* OpenLiteSpeed caching module is installed at the OLS server tier and overridden at the vhost tier. It is configured to expire items after 2 minutes, and is disabled entirely
by default. When caching is enabled, it is persisted for the Docker container as a bind mount in this project's /httpd/cache directory.

* When working with web-server tier settings/configuration, start by seeking to make changes at the vhost tier alone, and work your way up as needed. Priority of settings goes from
vhost context > vhost > server.

* Convenience scripts are available in the /docker directory and its subdirectories, for building, starting, stopping, and removing the docker containers, as well as "bashing in"
to a running container as root; there are also Docker Compose convenience scripts for bringing a stack up or down, and updating images with currently running containers.

* Currently OLS is configured to pass all ColdFusion template requests to Lucee via a "Web Proxy" External App configuration, as there was a bug in OLS that prevented connections
via AJP. The bug has been fixed in the OLS repo, and will be included in this Docker image in a future release.

* OpenLiteSpeed does NOT read .htaccess files. You can copy the rewrite rules of your .htaccess files to the vhost context in the OLS administrator. Be sure to do a graceful restart
of OLS when modifying rewrite rules.  You can browse to OLS Admin > Virtual Hosts > {hostname} > Context Tab > Static context > Rewrite Rules to see the current rules supporting SES
URLs, as an example of where to put 'em.

* Patience when restarting Tomcat: if you're doing it manually from inside the container, do the shutdown, then observe via TOP, wait for the java process to die first before
starting back up. If you start back up before the shutdown, the process goes unstable and you'll get 503s instead of your pages. You'll then need to shut Tomcat down and try again.

* SUPPORT:  This repo is provided as-is, but we are happy to help when possible. Please use the BitBucket Issues feature to report problems, or better yet, clone the repo and fix/enhance,
	and create a pull request - you'll be part of the solution! =)

