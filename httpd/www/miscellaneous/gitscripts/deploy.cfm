

<cfscript>

	setting requesttimeout="300";
	param url.release = "[UNSPECIFIED]";

	scriptDir		= GetDirectoryFromPath(getCurrentTemplatePath());
	scriptFile		= "deploy_production.sh";
	repoName		= "YOUR_REPO_NAME";
	repoUrl			= "YOUR_REPO_URL";
	notifyTo		= "admin@${ATCS_APP_DOMAIN}";
	notifyFrom		= "no-reply@${ATCS_APP_DOMAIN}";
	notifySubject	= "[${ATCS_APP_DOMAIN}] BitBucket Webhook Triggered";

	cfexecute( name				= "#scriptDir##scriptFile#"
	         , timeout			= "60"
	         , variable			= "deployResult"
	         );

	savecontent variable="htmlMessage" {
		WriteOutput('
			<h1> Push-Triggered Deployment <br /> Release: #url.release# </h1>
			<p> On #DateTimeFormat(Now())#, a push to BitBucket repository "<a href="#repoUrl#">#repoName#</a>"
				triggered a deployment to the production server. </p>
			<h2> Result: </h2>
			<pre> #deployResult# </pre>
		')
	}

	cfmail	( from		= "#notifyFrom#"
			, to		= "#notifyTo#"
			, subject	= "#notifySubject#"
			, type		= "html"
			)
			{ WriteOutput( htmlMessage );  }

</cfscript>

