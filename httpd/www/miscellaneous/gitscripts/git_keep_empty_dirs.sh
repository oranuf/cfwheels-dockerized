#!/bin/bash

# use this script to search for all empty directories in your project that you want git to pick up
# it'll drop a .keep file in each one for you.

git clean -nd | awk '{ print $3 }' | xargs -L1 -I{} touch {}.keep