

echo "<hr>"
echo "cd to project root"
cd ../../../../


echo "<hr>"
echo "git reset --hard origin/master"
git reset --hard origin/master


echo "<hr>"
echo "git fetch"
git fetch


echo "<hr>"
echo "git checkout -f origin/master"
git checkout -f origin/master


echo "<hr>"
echo "git status"
git status

#NOTE: if things break after the checkout from your repo, you may have permissions getting overwritten that you can fix at this point in the script.

echo "<hr>"
echo "END"

