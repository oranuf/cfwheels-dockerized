

<!---
	ABOUT:
		BitBucket can call a URL on your domain upon a PUSH to your repository. This script takes advantage of that to allow you to
		trigger a checkout of the master branch of your repo when you push a TAG with a name format of release_vX.X.XXX (where X's
		are your own incrementing version number). When the script sees that name in the webhook call's payload, it triggers a checkout
		of the branch, thereby updating your production environment code. This all happens outside of Docker images/containers, which
		is contrary to the typical Docker deployment methods of baking your new code into another image release, and deploying that.
	SETUP:
		Log in to your BitBucket account, and go to YOUR_REPO > Settings > Webhooks and add this script's url:
			http://${ATCS_APP_DOMAIN}/miscellaneous/gitscripts/bitbucket-webhook.cfm?action=deployProduction
	TESTING:
		{TODO}
	USE:
		Perform your usual development. Check out bug or feature branches, etc. and push them to your remote repo.
		The webhook will be called with each push to the remote repo, however it will not actually do anything 
		because you have not pushed any specially formatted tag to the remote repo yet.
		When you are ready to deploy to production, merge your branches into master, and push the master branch.
		Then, create a tag on your master branch, e.g. release_v1.2.300. Then push that tag to your remote repo.
		The webhook will run again, only this time the tag name will be part of the payload, and the script will
		execute the bash script to check out your master branch into the production environment, and your latest
		version of your code will go live at that time. You may want to code in additional functions that cause
		CFWheels to execute database migration scripts as necessary as this is not part of the current 
		configuration of this repo.
--->

<!--- TODO:	Make this script write a log to /httpd/logs/app/bitbucket-webhook.log --->
<!--- TODO: Make the /httpd/logs/ directories available via web browser, password protected --->


<cfscript>

	param url.action = "";
	param url.forceCheckout = "false";

	logDir = GetDirectoryFromPath(GetCurrentTemplatePath());
	logFile = "bitbucket_webhook_result.htm";
	urlDeployScript = "http://{THIS_APPS_DOMAIN}/miscellaneous/gitscripts/deploy.cfm";


	savecontent variable="html" {

		WriteOutput("<h1> Bitbucket Webhook Request Log</h1>");	
		WriteOutput("<h2> #DateTimeFormat(Now())# </h2>");
		WriteOutput("<p> Processing function: <strong>#url.action#</strong> </p>");

		if(url.action IS "deployProduction") {
			
			WriteOutput("<p> Forcing repository checkout?  <strong>#url.forceCheckout#</strong> </p>");

			strJson = "";
			strName = "";
			html = "";
			boolDeployToProduction = "false";

			if(!url.forceCheckout) {
				WriteOutput("<p> Parsing BitBucket payload... </p>");
				try {					
					strJson = toString( getHTTPRequestData().content );
			 		strJson = ReplaceNoCase( strJson, "payload=", "", "all" );
			 		strJson = URLDecode( strJson );
			 		objJson = DeserializeJSON( strJson );		 		
			 		WriteOutput("<p> <strong>Success.</strong> </p>");		 		
			 		WriteDump( label="BitBucket payload:", var="#objJson#", expand="false" );
			 		WriteOutput("<p> Parsing push name...");
			 		try {
			 			strName = objJson.push.changes[1].new.name;	
			 			WriteOutput("<p> <strong>Success.</strong> </p>");		 			 			
						WriteDump( label="Push name:", var="#strName#" );					
			 		} catch (any e) {
			 			WriteOutput("<p> Error parsing <strong>push name</strong>; may not exist on this push. </p>");	 			
			 		}			 		
				} catch (any e) {
					WriteOutput("<p> Error parsing <strong>bitbucket payload</strong>. </p>");	 			
					WriteDump( label="Parsing error details:", var="#cfcatch#" );
				}
			}

			boolIsRelease = (ListFirst(strName, "_") IS "release");
			boolDeployToProduction = ( boolIsRelease IS true || url.forceCheckout IS true);

			WriteOutput("<p> Deploy current branch to production? <strong>#boolDeployToProduction#</strong> </p>");

			if(boolDeployToProduction) {
				if (ListLen(strName, "_")) {
					strReleaseVersion = ListLast(strName, "_");
				} else {
					strReleaseVersion = "[FORCED_CHECKOUT]";
				}	
				WriteOutput("<p> Calling production deployment script for release <strong>#strReleaseVersion#</strong>; Review deployment script log for details. </p>");
				// TODO: Convert to POST, per REST API standards
				cfhttp(method="GET", url="#urlDeployScript#?release=#strReleaseVersion#", timeout="1"){};
			} else {
				WriteOutput("<p> Branch commit was <strong>not tagged as a release</strong>, and <strong>forceCheckout was false</strong>.</p>");
				WriteOutput("<p> Checkout was not completed, and <strong>production site remains unchanged</strong>. </p> ");
			}

		} else {
			WriteOutput("<p> Unknown function call. </p>");
		}

		WriteOutput("<p> Processing complete. </p>")

	}

	FileWrite("#logDir##logFile#", "#html#");

	WriteOutput("OK");	

</cfscript>


