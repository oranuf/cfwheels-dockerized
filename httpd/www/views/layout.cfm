<!--- Place HTML here that should be used as the default layout of your application. --->
<html>
  <head>
    <cfoutput>#csrfMetaTags()#</cfoutput>
  </head>

	<body>
		<cfoutput>
			<cfinclude template="/wheels/styles/header.cfm">		
			#flashMessages()#
			#includeContent()#
			<cfinclude template="/wheels/styles/footer.cfm">		
		</cfoutput>
	</body>
</html>
