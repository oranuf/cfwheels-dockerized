<cfscript>

	// Use this file to set variables for the Application.cfc's "this" scope.

	// Examples:
	// this.name = "MyAppName";
	// this.sessionTimeout = CreateTimeSpan(0,0,5,0);

	this.datasources["exampleapp"] = {
		  class: 'com.mysql.jdbc.Driver'
		, bundleName: 'com.mysql.jdbc'
		, bundleVersion: '5.1.38'
		, connectionString: 'jdbc:mysql://db:3306/exampleapp?useUnicode=true&characterEncoding=UTF-8&useLegacyDatetimeCode=true'
		, username: 'root'
		, password: "encrypted:909958b3f674300309a25ad1eb209af5a3301df0ed503f7b10e547777757773aaa0a49d880b06a50"
		
		// optional settings
		, clob:true // default: false
		, connectionLimit:100 // default:-1
	};

	this.datasource="exampleapp";

</cfscript>
